package com.thanhpd56;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by PHANTHANH on 8/18/2015.
 */
public class StringCalculator {

    private static final String defaultDelimiter = "(,|\n)";

    public static int add(String numbersString) {
        int result = 0;
        if (numbersString.startsWith("//")) {   // split with custom delimiters
            return sumWithCustomDelimiter(numbersString);
        } else {  // split with default delimiters
            return sumWithDefaultDelimiters(numbersString);
        }
    }

    // from Dung
    private static String escape(String regex) {
        final Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]");
        return SPECIAL_REGEX_CHARS.matcher(regex).replaceAll("\\\\$0");
    }

    private static String buildDelimiters(String text) {
        if (!text.startsWith("[")) return escape(text);
        Matcher matcher = Pattern.compile("\\[([^\\[\\]]+)+\\]").matcher(text);
        StringBuilder sb = new StringBuilder();

        while (matcher.find()) {
            if (sb.length() != 0)
                sb.append("|");
            String str = matcher.group(1);
            sb.append(escape(str));
        }
        return sb.toString();
    }

    private static int sumWithCustomDelimiter(String numbersString) {
        int startDelimiterIndex = numbersString.indexOf("//") + 2;
        int endDelimiterIndex = numbersString.indexOf("\n");
        String delimiters = numbersString.substring(startDelimiterIndex, endDelimiterIndex);
        String numbersStringWithoutSplitter = numbersString.substring(numbersString.indexOf("\n") + 1);

        // from Dungdm
        String formattedDelimiters = buildDelimiters(delimiters);
        return sumWithSpecifiedDelimiters(numbersStringWithoutSplitter, formattedDelimiters);
    }

    private static int sumWithDefaultDelimiters(String numbersString) {

        return sumWithSpecifiedDelimiters(numbersString, defaultDelimiter);
    }

    private static int sumWithSpecifiedDelimiters(String numbersString, String delimiters) {
        int result = 0;
        ArrayList<Integer> negativeNumbers = new ArrayList<Integer>();
        String[] numbers = numbersString.split(delimiters);
        for (String numberString : numbers) {
            if (!numberString.isEmpty()) {
                int n = Integer.parseInt(numberString);
                if (n <= 1000) {
                    if (n < 0) {
                        negativeNumbers.add(n);
                    }
                    result += n;
                }
            }
        }
        if (negativeNumbers.size() > 0) {
            String message = "negatives not allowed: " + negativeNumbers.toString();
            throw new RuntimeException(message);
        }
        return result;

    }
}
