package com.thanhpd56;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by PHANTHANH on 8/18/2015.
 */


public class StringCalculatorTest {

    // The method can take 0, 1, 2 numbers separated by commas
    @Test
    public void testActionAddWhenInputIsAnEmptyString_successTheReturns0() {
        Assert.assertEquals(0, StringCalculator.add(""));
    }


    @Test
    public void testActionAddWhenInputIsAStringOf2NumbersSeparatedByAComma_successThenReturnsSumOfThem() {
        Assert.assertEquals(3, StringCalculator.add("1,2"));
    }

    @Test
    public void testActionAddWhenInputIsAStringOfOneNumber_successThenReturnsIt() {
        Assert.assertEquals(5, StringCalculator.add("5"));
    }

    // Allow the Add method to handle an unknown amount of numbers

    @Test
    public void testActionAddWhenInputIsAnUnknownAmountOfNumber_successThenReturnsTheirSum() {
        Assert.assertEquals(1 + 2 + 3 + 4 + 5 + 6 + 7, StringCalculator.add("1,2,3,4,5,6,7"));
    }

    // Allow the Add method to handle new lines between numbers (instead of commas)
    @Test
    public void testActionAddWhenInputIsNumbersSeparatedByNewLine_successThenReturnsTheirSum() {
        Assert.assertEquals(1 + 2 + 3 + 4 + 5, StringCalculator.add("1,2\n3,4\n5"));
    }

    // Support different delimiters
    @Test
    public void testActionAddSWhenInputIsACustomDelimiterSyntax_successThenReturnsSumOfInputNumbers() {
        Assert.assertEquals(3, StringCalculator.add("//;\n1;2"));
    }


    // Ignore numbers which are greater than 1000
    @Test
    public void testActionAddWhenInputNumberIsGreaterThan1000_successThenItIsIgnored() {
        Assert.assertEquals(2, StringCalculator.add("2,1001"));
    }

    @Test
    public void testActionAddWhenInputNumberEqualsTo1000_successThenItIsIgnored() {
        Assert.assertEquals(1002, StringCalculator.add("2,1000"));
    }

    // Should accept delimiter with whatever size
    @Test
    public void testActionAddWhenInputIsACustomSizeDelimiter_successThenReturnsSumOfInputNumbers() {
        Assert.assertEquals(6, StringCalculator.add("//[***]\n1***2***3"));
    }

    @Test(expected = RuntimeException.class)
    public void testActionAddWhenInputContainsNegativeNumber_Failed() {
        StringCalculator.add("1,-2,-3,4");
    }

    @Test
    public void testActionAddWhenInputContainsNegativeNumbers_FailedThenThrowsException() {
        RuntimeException exception = null;
        try {
            StringCalculator.add("1,-2,-3,4");
        } catch (RuntimeException e) {
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertEquals(exception.getMessage(), "negatives not allowed: [-2, -3]");

    }

    // Allow different delimiters
    @Test
    public void testActionAddWhenInputIsAStringWithDifferentTypesOfDelimiters_successThenReturnsSumOfInputNumbers() {
        Assert.assertEquals(6, StringCalculator.add("//[*][%]\n1*2%3"));
    }


}
